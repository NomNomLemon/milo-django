"""untitled URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from application.profile.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', users_list, name='index'),
    url(r'^create/$', create_user, name='create'),
    url(r'^view/(?P<pk>\d+)/$', single_user, name='single'),
    url(r'^edit/(?P<pk>\d+)/$', edit_user, name='edit'),
    url(r'^delete/(?P<pk>\d+)/$', delete_user, name='delete'),
    url(r'^download/$', download, name='download'),
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
