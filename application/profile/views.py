import csv

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from django.views.generic.base import View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from application.profile.forms import ExtendedUserCreationForm, ExtendedUserEditForm
from application.profile.templatetags.profile_tags import bizz_or_fuzz, is_allowed


class CSVResponseMixin(object):

    def render_to_csv(self, data):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="user-list.csv"'

        writer = csv.writer(response)
        writer.writerow(['Username', 'Birthday', 'Eligible', 'Random Number', 'BizzFuzz'])
        for user in data:
            writer.writerow([user.username, user.profile.birth_date, is_allowed(user), user.profile.random_number,
                             bizz_or_fuzz(user)])

        return response


class UsersListView(ListView):
    model = User


class SingleUserView(DetailView):
    model = User


class CreateUserView(CreateView):
    model = User
    form_class = ExtendedUserCreationForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(CreateUserView, self).form_valid(form)


class EditUserView(UpdateView):
    model = User
    form_class = ExtendedUserEditForm
    success_url = reverse_lazy('index')


class DeleteUserView(DeleteView):
    model = User
    success_url = reverse_lazy('index')


class DownloadView(CSVResponseMixin, View):

    def get(self, request, *args, **kwargs):
        users = User.objects.all()
        return self.render_to_csv(users)


users_list = UsersListView.as_view()
single_user = SingleUserView.as_view()
create_user = CreateUserView.as_view()
edit_user = EditUserView.as_view()
delete_user = DeleteUserView.as_view()
download = DownloadView.as_view()
