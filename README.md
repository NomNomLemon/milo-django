Installation steps
============================================================
1. Create virtualenv
2. pip install requirements.txt
3. python manage.py migrate
4. python manage.py runserver

Notes
============================================================
1. Model for storing users profiles.
2. Views, templates and forms for listing all users, viewing, adding, editing and deleting a single user. 
3. Template tags for BizzFuzz and "allowed/blocked" functionality.
4. Donwload link for downloading users information in CSV format.

Project has no tests because it has primitive functionality, and I ran out of allowed time. I was working a little bit more three hours on this project.